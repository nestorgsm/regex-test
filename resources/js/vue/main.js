import { createApp } from "vue";

import App from "./App";
import router from "./router/router";
import MasonryWall from "@yeger/vue-masonry-wall";

createApp(App).use(router).use(MasonryWall).mount("#app");
