import { createRouter, createWebHistory } from "vue-router";

import Login from "../components/Login";
import Upload from "../components/Upload.vue";
import Images from "../components/Images";

export const routes = [
    // {
    //     name: "login",
    //     path: "/",
    //     component: Login,
    // },
    {
        name: "upload",
        path: "/",
        component: Upload,
    },
    {
        name: "images",
        path: "/images",
        component: Images,
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;
